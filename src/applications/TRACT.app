<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Gotransverse</label>
    <tabs>standard-Account</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Pricebook2</tabs>
    <tabs>standard-Product2</tabs>
    <tabs>Tract_Configure</tabs>
    <tabs>Marketplaces</tabs>
</CustomApplication>
